#include <stdio.h>

int Code[5];

void enterCode();
int testCode();

int main(int argc, char** argv){
	int correct = 2; // 1 true; 2 false
	printf("Welcome to masterMind\n ");
	enterCode();
	for(int n = 0; correct == 2 && n<10; n++){
		printf("This is the herrausforderres %d try of 10\n", n);
		int test = testCode();
		if(test == 1){
			correct = 1;
			printf("the masterMind Lost with the code\n %d %d %d %d %d\n", Code[0],Code[1],Code[2],Code[3],Code[4]);
		}
	}
	return 0;
}

void enterCode(){
	printf("masterMind enter the code(number by number)\n");
	for(int i = 0; i<5; ++i){
		scanf("%d", &Code[i]);
		fflush(stdin);
	}
	//system("cls"); under windows
	system("clear");
}

int testCode(){
	int rightWrong[5];
	for(int i =0; i<5; ++i){
		int n = 0;
		scanf("%d", &n);
	       	fflush(stdin);
       		if(n == Code[i]){
			rightWrong[i] = 0;
		}else{
			rightWrong[i] = 1;
		}
	}
	for(int i = 0; i<5; ++i){
		printf("%d", rightWrong[i]);
		if(i == 4)
			printf("\n");
	}
	for(int i = 0 ; i<5; ++i){
		if(rightWrong[i] == 1)
			return 2;
	}
	return 1;
}
	

